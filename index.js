import ModuleInsuranceScreens from './src';
import Home from './src/pages/Home';
import About from './src/pages/About';
import Settings from './src/pages/Settings';

export { ModuleInsuranceScreens, Home, About, Settings };