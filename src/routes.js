import { createStackNavigator } from 'react-navigation-stack';

import { createAppContainer } from 'react-navigation';

import Home from './pages/Home';
import Settings from './pages/Settings';
import About from './pages/About';

const StackNavigator = createStackNavigator(
  {
    About,
    Home,
    Settings,
  },
  {
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const RootStackContainer = createAppContainer(StackNavigator);

export default RootStackContainer;
