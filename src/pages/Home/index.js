import React from 'react';

import { Container, Title, Button, ButtonText } from './styles'

export default function Home({ navigation }) {
  return (
    <Container>
     <Title>Home - Modulo Seguros</Title>
     <Button onPress={() => navigation.navigate('Settings')}>
       <ButtonText>Ir para a Settings</ButtonText>
     </Button>
   </Container>
  );
}
