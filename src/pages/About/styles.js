import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: #1E90FF;
`;

export const Title = styled.Text`
  color: #FFF;
  font-size: 30px;
  font-weight: bold;
`;

export const Button = styled.TouchableOpacity`
  background-color: #00BFFF;
  border-radius: 3px;
  height: 50px;
  padding: 0px 20px;
  margin-top: 10px;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #FFF;
  font-size: 18px;
  font-weight: bold;
`;