import React from 'react';

import RootStackContainer from './routes';

export default function ModuleInsuranceScreens() {
  return (
    <RootStackContainer />
  );
}
